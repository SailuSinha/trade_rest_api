package com.citi.trade.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class TradeRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradeRestApiApplication.class, args);
	}

}
