package com.citi.trade.rest.classes;

public class Portfolio {
	private String ticker;
	private String name;
	private int quantity;
	private double value;
	
	public Portfolio(String ticker, String name) {
		super();
		this.ticker = ticker;
		this.name = name;
		this.quantity = 0;
		this.value = 0;
	}

	public String getTicker() {
		return ticker;
	}

	public void setTicker(String ticker) {
		this.ticker = ticker;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
	
}
