package com.citi.trade.rest.classes;

import java.util.Date;

public class Trade {
	public static enum status{
		CREATED,EXECUTED
	}
	public static enum executionStatus{
		PENDING,CANCELLED,REJECTED,FILLED,PARTIALLY_FILLED, ERROR
	}
	private Date date;
	private String stockTicker;
	private int quantity;
	private double price;
	private status status_;
	
	public Trade( String stockTicker, int quantity, double price) {
		super();
		this.date = new Date();
		this.stockTicker = stockTicker;
		this.quantity = quantity;
		this.price = price;
		this.status_ = status.CREATED;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getStockTicker() {
		return stockTicker;
	}
	public void setStockTicker(String stockTicker) {
		this.stockTicker = stockTicker;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public status getStatus() {
		return status_;
	}
	public void setStatus(status status_) {
		this.status_ = status_;
	}
	
}
