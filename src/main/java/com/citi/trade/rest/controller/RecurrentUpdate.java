package com.citi.trade.rest.controller;

import java.io.IOException;
import java.math.BigDecimal;

import org.bson.Document;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;

@Component
public class RecurrentUpdate {
	
	@Scheduled(fixedRate = 5000)
	public void reportCurrentTime() {
		Stock stock = null;
		double price = 0.0;
		try {
			stock = YahooFinance.get("AAPL");
			price = stock.getQuote().getPrice().doubleValue();
//			System.out.print("===== FUNCTION ENTERED ===="+price+"\n");
			
			MongoClient myMongo = new MongoClient();
			MongoDatabase database = myMongo.getDatabase("trade_db");
			MongoCollection<Document> mycollection = database.getCollection("portfolio");
			
			Document retrievePrice = new Document("price", price);
			mycollection.updateOne(Filters.eq("ticker", "AAPL"), new Document("$set", retrievePrice));
			myMongo.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
