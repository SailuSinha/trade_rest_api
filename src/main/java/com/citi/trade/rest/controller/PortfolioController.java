package com.citi.trade.rest.controller;

import java.io.IOException;
import java.util.Iterator;

import javax.xml.crypto.dsig.keyinfo.RetrievalMethod;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.citi.trade.rest.classes.Portfolio;
import com.citi.trade.rest.classes.Trade;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;

@RestController
@CrossOrigin
public class PortfolioController {
	@RequestMapping(value="/portfolio", method=RequestMethod.POST)
	public String createPortfolio(@RequestBody Portfolio portfolio) {

		MongoClient myMongo = new MongoClient();
		MongoDatabase database = myMongo.getDatabase("trade_db");
		
		Document newPortfolio = new Document("ticker", portfolio.getTicker()).append("name", portfolio.getName()).append("quantity",portfolio.getQuantity()).append("price", portfolio.getValue());
		MongoCollection<Document> mycollection = database.getCollection("portfolio");
		mycollection.insertOne(newPortfolio);
		myMongo.close(); 
		return "Portfolio created";
	}
	
	public static void update(String ticker, int qty, double price) {
		MongoClient myMongo = new MongoClient();
		MongoDatabase database = myMongo.getDatabase("trade_db");
		MongoCollection<Document> mycollection = database.getCollection("portfolio");
		
		Document retrieveTicker = mycollection.find(Filters.eq("ticker",ticker)).first();
		if(retrieveTicker == null) {
			try {
				Stock stock = YahooFinance.get(ticker);
				String name = stock.getName();
				Document newPortfolio = new Document("ticker", ticker).append("name", name).append("quantity",qty).append("price", price);
				mycollection.insertOne(newPortfolio);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}			
		else {
			int qtyVal = retrieveTicker.getInteger("quantity") + qty;
			Document retrieveQty = new Document("quantity",qtyVal).append("price", price);
			mycollection.updateOne(Filters.eq("ticker", ticker), new Document("$set", retrieveQty));
		}
		
		myMongo.close();
		return;
	}
}
